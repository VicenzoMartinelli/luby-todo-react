Crie um projeto em React JS chamado TodoLuby

A estilização (simples) deve ser com https://styled-components.com
O projeto deve ser SPA
Deve ser criado uma ferramenta todo list com as seguintes características
Um campo de texto para digitar a tarefa e um botão adicionar.
Implemente a validação no campo, para que a tarefa não seja incluída na lista sem conteúdo. A tarefa deve ter no mínimo 5 caracteres.
Gravar as tarefas em uma localstorage.

-Se possuir conhecimentos, utilize https://github.com/jaredpalmer/formik para criar o form. (Não obrigatório)
- Se possuir conhecimentos, utilize https://github.com/jquense/yup para validação (Não obrigatório)
- Se possuir conhecimentos em redux, utilize reducers e actions para a tarefa de add e list. (Não obrigatório)
- Criar botão para ordenar a lista, (Não obrigatório)
- Se possível utilize o next js para renderizar a página. (Não obrigatório)

Não é necessário criar funções para apagar ou editar para o teste.
Não se preocupe com formatação visual, queremos avaliar seu conhecimento em React e lógica em tarefas simples.

Após a conclusão, suba em um repositório bitbucket e convide os usuários rodrigo@astl.com.br e tiago-lopes@outlook.com
