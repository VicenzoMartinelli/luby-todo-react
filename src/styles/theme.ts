const theme = {
  colors: {
    background: '#121214',
    inputBackground: '#212025',
    text: '#e1e1e6',
    primary: '#02a4ef',
    danger: '#f14141'
  }
}

export default theme
