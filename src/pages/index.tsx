import React from 'react'
import Head from 'next/head'
import { TodoProvider } from '../components/Home/TodoContext'
import TodoMain from '../components/Home/TodoMain'
import { FullContainer } from '../components/Common/FullContainer'
import { Title } from '../components/Common/Title'
import { MainImage } from '../components/Common/MainImage'

const Home: React.FC = () => {
  return (
    <FullContainer>
      <Head>
        <title>Luby Todo</title>
      </Head>

      <MainImage src="https://www.luby.com.br/wp-content/uploads/2020/05/Logo_Luby.png" />
      <Title>To do Luby</Title>
      <TodoProvider>
        <TodoMain />
      </TodoProvider>
    </FullContainer>
  )
}

export default Home
