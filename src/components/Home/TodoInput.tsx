import styled from 'styled-components'
import React, { useCallback } from 'react'
import { CheckOutlined, VerticalAlignBottomOutlined } from '@ant-design/icons'
import { useField, useFormikContext } from 'formik'
import { TodoFormValues } from './TodoMain'
import { useTodoStore, TodoActionType } from './TodoContext'

const TodoInputContainer = styled.div`
  flex: 1;

  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`

const TodoInputRow = styled.div<{ justify?: string }>`
  flex: 1;
  width: 100%;
  display: flex;
  justify-content: ${props => props.justify || 'center'};
  align-items: center;
  flex-direction: row;
`

const TodoInternalInput = styled.input`
  background-color: ${t => t.theme.colors.inputBackground};
  color: ${t => t.theme.colors.text};
  flex: 5;
  border-radius: 15px;
  padding: 16px;
  border-radius: 15px;
  font-size: 16px;
  border: none;
  outline: none;
`

const TodoSubmitButton = styled.button`
  background-color: ${t => t.theme.colors.primary};
  color: ${t => t.theme.colors.text};
  border-radius: 50%;
  width: 50px;
  flex: none;
  height: 50px;
  outline: none;
  font-size: 24px;
  margin-right: 8px;
  margin-left: 8px;
  border: none;
  cursor: pointer;
  transition: 200ms background-color linear;

  &:hover {
    background-color: ${t => t.theme.colors.text};
    color: ${t => t.theme.colors.primary};
  }
`

const TodoOrderButton = styled.button`
  background-color: ${t => t.theme.colors.primary};
  color: ${t => t.theme.colors.text};
  border-radius: 50%;
  font-size: 24px;
  width: 50px;
  flex: none;
  height: 50px;
  outline: none;
  border: none;
  cursor: pointer;
  margin-right: 8px;
  margin-left: 8px;
  transition: 200ms background-color linear;

  &:hover {
    background-color: ${t => t.theme.colors.text};
    color: ${t => t.theme.colors.primary};
  }
`

const TodoInputError = styled.label`
  color: ${t => t.theme.colors.danger};
  font-size: 14px;
  margin-left: 4px;
  margin-top: 4px;
  margin-bottom: 4px;
`

export function TodoInput({ name }) {
  const { dispatch } = useTodoStore()

  const { errors } = useFormikContext<TodoFormValues>()
  const [field] = useField({
    name: name
  })

  const handleOrderTodos = useCallback(() => {
    dispatch({
      type: TodoActionType.ORDER,
      arguments: null
    })
  }, [dispatch])

  return (
    <TodoInputContainer>
      <TodoInputRow>
        <TodoInternalInput
          type="text"
          placeholder="Cadastre seu to do"
          autoFocus
          {...field}
        />
        <TodoSubmitButton type="submit">
          <CheckOutlined />
        </TodoSubmitButton>

        <TodoOrderButton type="button" onClick={handleOrderTodos}>
          <VerticalAlignBottomOutlined />
        </TodoOrderButton>
      </TodoInputRow>
      <TodoInputRow justify="flex-start">
        {errors.name && <TodoInputError>{errors.name}</TodoInputError>}
      </TodoInputRow>
    </TodoInputContainer>
  )
}
