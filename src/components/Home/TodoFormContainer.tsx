import React from 'react'
import styled from 'styled-components'
import { Form } from 'formik'

export const FormContainer = styled(Form)`
  width: 80%;
  margin: 0 auto;
  padding: 16px 0 8px 0;
  display: flex;
  align-items: start;
  justify-content: space-between;
`