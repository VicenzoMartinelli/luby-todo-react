import React, { createContext, useContext, useReducer, Dispatch, useEffect } from 'react';

interface Todo {
    name: string;
}

interface TodoState {
    todos: Todo[];
    dispatch?: Dispatch<TodoActionDescriber> | null;
}

interface TodoActionDescriber {
    type: TodoActionType;
    arguments: any | null;
}

export enum TodoActionType {
    LIST,
    ADD,
    ADD_MANY,
    REMOVE,
    ORDER
}

const localStorageKeys = {
    todos: 'LUBY-TODOS'
}

const initialState: TodoState = {
    todos: []
}

const store = createContext(initialState)

const { Provider } = store

const TodoProvider = ({ children }) => {
    const [state, dispatch] = useReducer((state: TodoState, action: TodoActionDescriber) => {
        let newState = state;

        switch (action.type) {
            case TodoActionType.LIST: {
                return state;
            }
            case TodoActionType.ADD: {
                newState = { todos: [...state.todos, action.arguments] };
                break;
            }
            case TodoActionType.ADD_MANY: {
                newState = { todos: [...state.todos, ...action.arguments] };
                break;
            }
            case TodoActionType.REMOVE: {
                newState = { todos: state.todos.filter(t => t.name !== action.arguments.name) };
                break;
            }
            case TodoActionType.ORDER: {
                newState = { todos: state.todos.sort((a, b) => a.name.localeCompare(b.name)) };
                break;
            }
            default:
                throw new Error();
        };
        localStorage.setItem(localStorageKeys.todos, JSON.stringify(newState));
        return newState;
    }, initialState);

    useEffect(() => {
        const restoredState: TodoState = JSON.parse(localStorage.getItem(localStorageKeys.todos))

        dispatch({
            type: TodoActionType.ADD_MANY,
            arguments: restoredState?.todos ?? []
        })
    }, []);

    return <Provider value={{ todos: state.todos, dispatch }}>{children}</Provider>;
};

const useTodoStore = () => useContext(store);

export { store, TodoProvider, useTodoStore }
