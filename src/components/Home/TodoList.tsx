import styled from 'styled-components'
import React, { useCallback } from 'react'
import { RightOutlined, DeleteOutlined } from '@ant-design/icons'
import { useTodoStore, TodoActionType } from './TodoContext'

const TodoListContainer = styled.div`
  width: 80%;
  margin: 0 auto;
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: start;
  background-color: #212025;
  border-radius: 15px;
  align-items: flex-start;
`

const TodoListItem = styled.div`
  flex: 1;

  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: row;
  padding: 16px;
`
const TodoListItemLeft = styled.div`
  flex: 6;
  display: flex;
  justify-content: flex-start;
  align-items: center;
`

const TodoListItemLeftIcon = styled(RightOutlined)`
  margin-right: 8px;
`

const TodoListItemRight = styled.div`
  flex: 1;
  display: flex;
  justify-content: flex-end;
  align-items: center;
`

const TodoListItemRemoveButton = styled.button`
  background: transparent;
  color: ${t => t.theme.colors.danger};
  font-size: 16px;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 32px;
  height: 32px;
  outline: none;
  border: none;
  cursor: pointer;
`

export function TodoList() {
  const { todos, dispatch } = useTodoStore()

  const onDeleteClick = useCallback(
    name => {
      dispatch({
        arguments: {
          name
        },
        type: TodoActionType.REMOVE
      })
    },
    [dispatch]
  )

  return (
    <TodoListContainer>
      {todos.map((item, index) => (
        <TodoListItem key={index}>
          <TodoListItemLeft>
            <TodoListItemLeftIcon />
            {item.name}
          </TodoListItemLeft>
          <TodoListItemRight>
            <TodoListItemRemoveButton onClick={() => onDeleteClick(item.name)}>
              <DeleteOutlined />
            </TodoListItemRemoveButton>
          </TodoListItemRight>
        </TodoListItem>
      ))}
    </TodoListContainer>
  )
}
