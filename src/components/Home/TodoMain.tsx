import React from 'react'
import { Formik } from 'formik'
import * as yup from 'yup'
import { useTodoStore, TodoActionType } from './TodoContext'
import { TodoInput } from './TodoInput'
import { TodoList } from './TodoList'
import { FormContainer } from './TodoFormContainer'

const validationSchema = yup.object({
  name: yup
    .string()
    .min(5, 'O nome da tarefa deve possuir no mínimo 5 caracteres')
    .required('Insira o nome da tarefa')
})

export interface TodoFormValues {
  name: string
}

const TodoMain: React.FC = () => {
  const { dispatch } = useTodoStore()

  const initialValues: TodoFormValues = {
    name: ''
  }

  return (
    <>
      <Formik
        validationSchema={validationSchema}
        initialValues={initialValues}
        onSubmit={(values, { resetForm }) => {
          dispatch({
            type: TodoActionType.ADD,
            arguments: values
          })

          resetForm()
        }}
      >
        {() => (
          <FormContainer>
            <TodoInput name="name" />
          </FormContainer>
        )}
      </Formik>
      <TodoList />
    </>
  )
}

export default TodoMain
