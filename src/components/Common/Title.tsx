import styled from 'styled-components'

export const Title = styled.h3`
  color: ${t => t.theme.colors.primary};
  font-weight: 700;
  text-align: center;
  font-size: 24px;
`
